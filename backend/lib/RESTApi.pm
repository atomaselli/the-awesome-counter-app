package RESTApi;
use Dancer2; 
use Dancer2::Plugin::ParamTypes;
use Cache::Memcached::Fast;


our $VERSION = '0.2';

set serializer => 'JSON';



get '/' => sub {


my $memcache_srv = $ENV{'MEMCACHE_SERVICE_HOST'}.":11211";


my $key = 'counter';
my $memd = Cache::Memcached::Fast->new({
    servers => [
             { address => $memcache_srv }, 
             
               ],

    connect_timeout => 0.2,
    io_timeout => 0.5,
    close_on_error => 1,
    max_failures => 3,
    failure_timeout => 2,
    ketama_points => 150,
    nowait => 1,
    utf8 => 1,
    max_size => 512 * 1024,
});

if (!$memd->get( $key ))
    {
    $memd->set( $key, 0 );
    }

$memd->incr( $key );

push_header( 'X-Server-ID' => $ENV{'HOSTNAME'} );

return { 
        'status'   => '200',
        'counter'  => $memd->get( $key ) || 'undef',
        'hostname' => $ENV{'HOSTNAME'}
       }
};

true;