package RESTApi;
use Dancer2; 
use Dancer2::Plugin::ParamTypes;
use LWP::UserAgent;
#use Cpanel::JSON::XS;


our $VERSION = '0.2';


get '/' => sub {

my $backend_srv = $ENV{'BACKEND_SERVICE_HOST'}.":8080";


my $ua = 'LWP::UserAgent'->new(send_te => 0);
my $req = 'HTTP::Request'->new(
        GET => "http://".$backend_srv."/",
        [
            'Accept'        => '*/*',
            'Content-Type'  => 'application/json',
        ],

    );

my $res = $ua->request($req);
die $res->status_line unless $res->is_success;
my $response_structure = decode_json($res->decoded_content);


    my $vars = {
        'hostname' => $ENV{'HOSTNAME'},
        'counter'  => $response_structure->{'counter'},
        'backend'  => $response_structure->{'hostname'},
    };

    content_type 'application/json';


    template 'index' => { 'var' => $vars,  };
};

true;